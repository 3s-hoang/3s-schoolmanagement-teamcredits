﻿using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.Models.Entity.User;
using CreditTeam.Repository.GenericRepository;
using CreditTeam.Repository.SchoolRepository;
using CreditTeam.Service.SchoolService;
using CreditTeam.UnitOfWork;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace CreditTeam
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            //mapping
            var config = new AutoMapper.MapperConfiguration(c => { c.AddProfile(new MapProfile()); }
            );
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<TeamCreditDbContext>(item =>
                item.UseSqlServer(Configuration.GetConnectionString("myconn")));

            
            services.AddIdentity<User, Role>(options =>
                {
                    options.Password.RequiredLength = 6;
                    options.Password.RequireUppercase = true;
                    options.Password.RequireLowercase = true;
                    options.Password.RequireNonAlphanumeric = true;
                    options.Password.RequireDigit = true;
                })
                .AddEntityFrameworkStores<TeamCreditDbContext>()
                .AddDefaultTokenProviders();
            
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Account/Login";
                options.AccessDeniedPath = "/Account/Error404";
                options.SlidingExpiration = true;
            });
            //Authorization policies
            services.AddAuthorization(options =>
            {
                options.AddPolicy("readonlypolicy",
                    builder => builder.RequireRole("Admin", "Student", "Teacher"));
                options.AddPolicy("writepolicy",
                    builder => builder.RequireRole("Admin"));
                options.AddPolicy("userprofilepolicy",
                    builder => builder.RequireRole("Student","Teacher"));
                options.AddPolicy("pointpolicy",
                    builder => builder.RequireRole("Teacher"));
                options.AddPolicy("transcriptspolicy",
                    builder => builder.RequireRole("Student"));
            });
            services.AddScoped<IUnitOfWork, UnitOfWork.UnitOfWork>();
            //Class Room
            services.AddTransient<IClassRoomRepository, ClassRoomRepository>();
            services.AddScoped<IClassRoomService, ClassRoomService>();
            services.AddTransient<IGenericRepository<ClassRoom>, GenericRepository<ClassRoom>>();
            //Faculty
            services.AddTransient<IFacultyRepository, FacultyRepository>();
            services.AddScoped<IFacultyService, FacultyService>();
            services.AddTransient<IGenericRepository<Faculty>, GenericRepository<Faculty>>();
            //Discipline
            services.AddTransient<IDisciplineRepository, DisciplineRepository>();
            services.AddScoped<IDisciplineService, DisciplineService>();
            services.AddTransient<IGenericRepository<Discipline>, GenericRepository<Discipline>>();
            //Point
            services.AddTransient<IPointRepository, PointRepository>();
            services.AddScoped<IPointService, PointService>();
            services.AddTransient<IGenericRepository<Point>, GenericRepository<Point>>();
            //Semester
            services.AddTransient<ISemesterRepository, SemesterRepository>();
            services.AddScoped<ISemesterService, SemesterService>();
            services.AddTransient<IGenericRepository<Semester>, GenericRepository<Semester>>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}