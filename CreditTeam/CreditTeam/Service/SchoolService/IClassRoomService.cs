﻿using System.Collections.Generic;
using CreditTeam.Models.Entity.School;

namespace CreditTeam.Service.SchoolService
{
    public interface IClassRoomService
    {
        ClassRoom GetById(int id);
        IEnumerable<ClassRoom> GetAll();
        void Add(ClassRoom classRoom);
        void Update(ClassRoom classRoom);
        void Delete(int id);
        void Save();
        ClassRoom FindById(string classroomid);
    }
}