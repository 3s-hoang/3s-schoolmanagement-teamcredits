﻿using System.Collections.Generic;
using CreditTeam.Models.Entity.School;

namespace CreditTeam.Service.SchoolService
{
    public interface IDisciplineService 
    {
        Discipline GetById(int id);
        IEnumerable<Discipline> GetAll();
        void Add(Discipline discipline);
        void Update(Discipline discipline);
        void Delete(int id);
        void Save();

    }
}