﻿using System.Collections.Generic;
using CreditTeam.Models.Entity.School;

namespace CreditTeam.Service.SchoolService
{
    public interface IPointService
    {
        Point GetById(int id);
        IEnumerable<Point> GetAll();
        void Add(Point point);
        void Update(Point point);
        void Delete(int id);
        void Save();
    }
}