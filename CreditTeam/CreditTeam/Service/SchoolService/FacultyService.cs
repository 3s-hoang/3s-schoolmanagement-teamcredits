﻿using System.Collections.Generic;
using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace CreditTeam.Service.SchoolService
{
    public class FacultyService : IFacultyService
    {
        private IUnitOfWork _unitOfWork;
        private TeamCreditDbContext _context;

        public FacultyService(IUnitOfWork unitOfWork, TeamCreditDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }

        public Faculty GetById(int id)
        {
            return _unitOfWork.FacultyRepository.GetById(id);
        }

        public IEnumerable<Faculty> GetAll()
        {
            IEnumerable<Faculty> faculties = _context.Faculties.Include(d => d.ClassRoom);
            return faculties;
        }

        public void Add(Faculty faculty)
        {
            _unitOfWork.FacultyRepository.Add(faculty);
        }

        public void Update(Faculty faculty)
        {
            _unitOfWork.FacultyRepository.Update(faculty);
        }

        public void Delete(int id)
        {
            _unitOfWork.FacultyRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.FacultyRepository.Save();
        }
    }
}