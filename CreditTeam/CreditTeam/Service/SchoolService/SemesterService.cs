﻿using System.Collections.Generic;
using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.UnitOfWork;

namespace CreditTeam.Service.SchoolService
{
    public class SemesterService : ISemesterService

    {
        private IUnitOfWork _unitOfWork;
        private TeamCreditDbContext _context;

        public SemesterService(IUnitOfWork unitOfWork, TeamCreditDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }

        public Semester GetById(int id)
        {
            return _unitOfWork.SemesterRepository.GetById(id);
        }

        public IEnumerable<Semester> GetAll()
        {
            return _unitOfWork.SemesterRepository.GetAll();
        }

        public void Add(Semester semester)
        {
            _unitOfWork.SemesterRepository.Add(semester);
        }

        public void Update(Semester semester)
        {
            _unitOfWork.SemesterRepository.Update(semester);
        }

        public void Delete(int id)
        {
            _unitOfWork.SemesterRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.SemesterRepository.Save();
        }
    }
}