﻿using System.Collections.Generic;
using CreditTeam.Models.Entity.School;

namespace CreditTeam.Service.SchoolService
{
    public interface ISemesterService
    {
        Semester GetById(int id);
        IEnumerable<Semester> GetAll();
        void Add(Semester semester);
        void Update(Semester semester);
        void Delete(int id);
        void Save();
    }
}