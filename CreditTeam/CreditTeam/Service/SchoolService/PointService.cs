﻿using System.Collections.Generic;
using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.UnitOfWork;
using Microsoft.EntityFrameworkCore;

namespace CreditTeam.Service.SchoolService
{
    public class PointService : IPointService
    {
        private IUnitOfWork _unitOfWork;
        private TeamCreditDbContext _context;

        public PointService(IUnitOfWork unitOfWork, TeamCreditDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }

        public Point GetById(int id)
        {
            return _unitOfWork.PointRepository.GetById(id);
        }

        public IEnumerable<Point> GetAll()
        {
            IEnumerable<Point> points = _context.Points.Include(d => d.Discipline);
            return points;
        }

        public void Add(Point point)
        {
            _unitOfWork.PointRepository.Add(point);
        }

        public void Update(Point point)
        {
            _unitOfWork.PointRepository.Update(point);
        }

        public void Delete(int id)
        {
            _unitOfWork.PointRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.PointRepository.Save();
        }
    }
}