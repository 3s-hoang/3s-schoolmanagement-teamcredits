﻿using System.Collections.Generic;
using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.UnitOfWork;

namespace CreditTeam.Service.SchoolService
{
    public class ClassRoomService : IClassRoomService
    {
        private IUnitOfWork _unitOfWork;
        private TeamCreditDbContext _context;

        public ClassRoomService(IUnitOfWork unitOfWork, TeamCreditDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }

        public ClassRoom GetById(int id)
        {
            return _unitOfWork.ClassRoomRepository.GetById(id);
        }

        public IEnumerable<ClassRoom> GetAll()
        {
            return _unitOfWork.ClassRoomRepository.GetAll();
        }

        public void Add(ClassRoom classRoom)
        {
            _unitOfWork.ClassRoomRepository.Add(classRoom);
        }

        public void Update(ClassRoom classRoom)
        {
            _unitOfWork.ClassRoomRepository.Update(classRoom);
        }

        public void Delete(int id)
        {
            _unitOfWork.ClassRoomRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.ClassRoomRepository.Save();
        }

        public ClassRoom FindById(string classroomid)
        {
            return _context.ClassRooms.Find(classroomid);
        }
    }
}