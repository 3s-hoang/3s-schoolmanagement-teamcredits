﻿using System.Collections.Generic;
using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.UnitOfWork;
using Microsoft.EntityFrameworkCore;
namespace CreditTeam.Service.SchoolService
{
    public class DisciplineService : IDisciplineService
    {
        private IUnitOfWork _unitOfWork;
        private TeamCreditDbContext _context;

        public DisciplineService(IUnitOfWork unitOfWork, TeamCreditDbContext context)
        {
            _unitOfWork = unitOfWork;
            _context = context;
        }

        public Discipline GetById(int id)
        {
            return _unitOfWork.DisciplineRepository.GetById(id);
        }

        IEnumerable<Discipline> IDisciplineService.GetAll()
        {
            IEnumerable<Discipline> disciplines = _context.Disciplines.Include(d => d.Semester);
            return disciplines;
        }

        public void Add(Discipline discipline)
        {
            _unitOfWork.DisciplineRepository.Add(discipline);
        }

        public void Update(Discipline discipline)
        {
            _unitOfWork.DisciplineRepository.Update(discipline);
        }

        public void Delete(int id)
        {
            _unitOfWork.DisciplineRepository.Delete(id);
        }

        public void Save()
        {
            _unitOfWork.DisciplineRepository.Save();
        }
    }
}