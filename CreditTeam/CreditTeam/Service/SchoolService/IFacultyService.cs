﻿using System.Collections.Generic;
using CreditTeam.Models.Entity.School;

namespace CreditTeam.Service.SchoolService
{
    public interface IFacultyService
    {
        Faculty GetById(int id);
        IEnumerable<Faculty> GetAll();
        void Add(Faculty faculty);
        void Update(Faculty faculty);
        void Delete(int id);
        void Save();
    }
}