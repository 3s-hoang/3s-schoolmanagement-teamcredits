﻿using AutoMapper;
using CreditTeam.Models.Entity.School;
using CreditTeam.Models.Entity.User;
using CreditTeam.Models.ViewModel.School;
using CreditTeam.Models.ViewModel.User;

namespace CreditTeam.Models
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            //Classroom
            CreateMap<ClassRoom, ClassRoomListViewModel>();
            CreateMap<ClassRoomListViewModel, ClassRoom>();
            CreateMap<ClassRoom, ClassRoomAddEditViewModel>();
            CreateMap<ClassRoomAddEditViewModel, ClassRoom>();
            //Faculty
            CreateMap<Faculty, ListFacultyViewModel>();
            CreateMap<ListFacultyViewModel, Faculty>();
            CreateMap<Faculty, AddEditFacultyViewModel>();
            CreateMap<AddEditFacultyViewModel, Faculty>();
            //Semeseter
            CreateMap<Semester, ListSemesterViewModel>();
            CreateMap<ListSemesterViewModel, Semester>();
            CreateMap<Semester, AddEditSemesterViewModel>();
            CreateMap<AddEditSemesterViewModel, Semester>();
            //DipCipline
            CreateMap<Discipline, ListDisciplineViewModel>();
            CreateMap<ListDisciplineViewModel, Discipline>();
            CreateMap<Discipline, AddEditDisciplineViewModel>();
            CreateMap<AddEditDisciplineViewModel, Discipline>();
            //Point
            CreateMap<Point, AddEditPointViewModel>();
            CreateMap<AddEditPointViewModel, Point>();
            CreateMap<Point, EditPointViewModel>();
            CreateMap<EditPointViewModel, Point>();
        }
    }
}