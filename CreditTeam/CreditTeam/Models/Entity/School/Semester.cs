﻿using System.Collections.Generic;

namespace CreditTeam.Models.Entity.School
{
    public class Semester
    {
        public int Id { get; set; }
        public string Name { get; set; }

        //disciplines
        public ICollection<Discipline> Disciplines { get; set; }
    }
}