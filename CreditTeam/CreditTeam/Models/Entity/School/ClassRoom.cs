﻿using System.Collections.Generic;

namespace CreditTeam.Models.Entity.School
{
    public class ClassRoom
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public ICollection<User.User> Users { get; set; }
    }
}