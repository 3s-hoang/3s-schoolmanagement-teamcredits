﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CreditTeam.Models.Entity.School
{
    public class Discipline
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Unit { get; set; }

        //semester
        public int SemesterId { get; set; }
        [ForeignKey("SemesterId")] public Semester Semester { get; set; }

        //point
        public ICollection<Point> Points { get; set; }
    }
}