﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace CreditTeam.Models.Entity.School
{
    public class Faculty
    {
        public int Id { get; set; }
        public string Name { get; set; }

        //classroom
        public int ClassRoomId { get; set; }
        [ForeignKey("ClassRoomId")] public ClassRoom ClassRoom { get; set; }

        //user
        public ICollection<User.User> Users { get; set; }
    }
}