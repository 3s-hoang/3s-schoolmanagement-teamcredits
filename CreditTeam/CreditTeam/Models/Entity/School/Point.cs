﻿using System.ComponentModel.DataAnnotations.Schema;

namespace CreditTeam.Models.Entity.School
{
    public class Point
    {
        public int Id { get; set; }
        public float ComponentScore { get; set; }
        public float FinalExam0 { get; set; }
        //can be null
        public float FinalExam1 { get; set; }
        public float ScoreByScale010 { get; set; }
        public float ScoreByScale04 { get; set; }
        public string ScoreByabcdf { get; set; }

        //discipline
        public int DisciplineId { get; set; }
        [ForeignKey("DisciplineId")] public Discipline Discipline { get; set; }

        //user
        public int UserId { get; set; }
        [ForeignKey("UserId")] public User.User User { get; set; }
    }
}