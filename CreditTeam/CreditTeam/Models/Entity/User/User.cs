﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using CreditTeam.Models.Entity.School;
using Microsoft.AspNetCore.Identity;

namespace CreditTeam.Models.Entity.User
{
    public class User : IdentityUser<int>
    {
        public string FullName { get; set; }
        public string Sex { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public bool IsActive { get; set; }
        public string Image { get; set; }
        //classroom
        public int? ClassRoomId { get; set; }

        [ForeignKey("ClassRoomId")] public ClassRoom ClassRoom { get; set; }
        //Faculty
        
        public int? FacultyId { get; set; }
        [ForeignKey("FacultyId")] public Faculty Faculty { get; set; }
    }
}