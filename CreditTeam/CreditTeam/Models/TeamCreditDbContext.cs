﻿using CreditTeam.Models.Entity.School;
using CreditTeam.Models.Entity.User;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace CreditTeam.Models
{
    public class TeamCreditDbContext : IdentityDbContext<User,Role,int>
    {
        public TeamCreditDbContext(DbContextOptions<TeamCreditDbContext> options) : base(options){}
        
        public DbSet<ClassRoom> ClassRooms { get; set; }
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Discipline> Disciplines { get; set; }
        public DbSet<Semester> Semesters { get; set; }
        public DbSet<Point> Points { get; set; }
    }
}