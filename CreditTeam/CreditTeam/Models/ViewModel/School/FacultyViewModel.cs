﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CreditTeam.Models.Entity.School;

namespace CreditTeam.Models.ViewModel.School
{
    public class ListFacultyViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        //classroom
        [Required] public int ClassRoomId { get; set; }
        [ForeignKey("ClassRoomId")] public ClassRoom ClassRoom { get; set; }
    }

    public class AddEditFacultyViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        //classroom
        [Required] public int ClassRoomId { get; set; }
        [ForeignKey("ClassRoomId")] public ClassRoom ClassRoom { get; set; }
    }
}