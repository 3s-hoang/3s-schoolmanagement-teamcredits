﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CreditTeam.Models.Entity.School;

namespace CreditTeam.Models.ViewModel.School
{
    public class ListPointViewModel
    {
        public int Id { get; set; }
        public float ComponentScore { get; set; }
        public float FinalExam0 { get; set; }
        //can be null
        public float FinalExam1 { get; set; }
        public float ScoreByScale010 { get; set; }
        public float ScoreByScale04 { get; set; }
        public string ScoreByabcdf { get; set; }

        //discipline
        public string DisciplineName { get; set; }

        //user
        public string StudentName { get; set; }
    }

    public class AddEditPointViewModel
    {
        public int Id { get; set; }

        [Required]
        [Range(0, 10.0, ErrorMessage = "ComponentScore must be between 0 and 10.0")]
        public float ComponentScore { get; set; }

        [Required]
        [Range(0, 10.0, ErrorMessage = "ComponentScore must be between 0 and 10.0")]
        public float FinalExam0 { get; set; }

        //can be null
        public float? FinalExam1 { get; set; }

        [Required]
        [Range(0, 10.0, ErrorMessage = "ScoreByScale010 must be between 0 and 10.0")]
        public float ScoreByScale010 { get; set; }

        [Required]
        [Range(0, 4, ErrorMessage = "scoreByScale04 must be between 0 and 4")]
        public float ScoreByScale04 { get; set; }

        [Required]
        [RegularExpression(@"^[abcdfABCDF]{1}$", ErrorMessage = "ScoreByabcdf must be between a-f or A-F")]
        public string ScoreByabcdf { get; set; }

        //discipline
        [Required] public int DisciplineId { get; set; }
        [ForeignKey("DisciplineId")] public Discipline Discipline { get; set; }

        //user
        [Required] public int UserId { get; set; }
        [ForeignKey("UserId")] public Entity.User.User User { get; set; }
    }

    public class EditPointViewModel
    {
        
        public int Id { get; set; }

        [Required]
        [Range(0, 10.0, ErrorMessage = "ComponentScore must be between 0 and 10.0")]
        public float ComponentScore { get; set; }

        [Required]
        [Range(0, 10.0, ErrorMessage = "ComponentScore must be between 0 and 10.0")]
        public float FinalExam0 { get; set; }

        //can be null
        public float? FinalExam1 { get; set; }

        [Required]
        [Range(0, 10.0, ErrorMessage = "ScoreByScale010 must be between 0 and 10.0")]
        public float ScoreByScale010 { get; set; }

        [Required]
        [Range(0, 4, ErrorMessage = "scoreByScale04 must be between 0 and 4")]
        public float ScoreByScale04 { get; set; }

        [Required]
        [RegularExpression(@"^[abcdfABCDF]{1}$", ErrorMessage = "ScoreByabcdf must be between a-f or A-F")]
        public string ScoreByabcdf { get; set; }
    }
}