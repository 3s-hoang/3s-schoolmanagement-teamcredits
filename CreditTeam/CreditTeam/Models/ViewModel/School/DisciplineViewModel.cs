﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CreditTeam.Models.Entity.School;

namespace CreditTeam.Models.ViewModel.School
{
    public class ListDisciplineViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Unit { get; set; }

        //semester
        [Required] public int SemesterId { get; set; }
        [ForeignKey("SemesterId")] public Semester Semester { get; set; }
    }

    public class AddEditDisciplineViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Unit is required")]
        [Range(1, 10, ErrorMessage = "Unit must be between 1 and 10")]
        public int Unit { get; set; }

        //semester
        [Required] public int SemesterId { get; set; }
        [ForeignKey("SemesterId")] public Semester Semester { get; set; }
    }
}