﻿using System.ComponentModel.DataAnnotations;

namespace CreditTeam.Models.ViewModel.School
{
    public class ClassRoomListViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ClassRoomAddEditViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
    }
}