﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CreditTeam.Models.Entity.School;

namespace CreditTeam.Models.ViewModel.User
{

    public class RegisterViewModel
    {   
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string RoleName { get; set; }
        
        [Required]
        public string FullName { get; set; }
        public string Sex { get; set; }
        [Required] public string Address { get; set; }
        public DateTime DateBirth { get; set; }
        public bool IsActive { get; set; } 

        [RegularExpression(@"([03|08|07])+([0-9]{8})")]
        public string PhoneNumber { get; set; }
        
        public int? ClassRoomId { get; set; }
        
        public int? FacultyId { get; set; }
    }

    public class LoginViewModel
    {
        [Required] public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")] public bool RememberMe { get; set; }
    }

    public class UserProfileViewModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Sex { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true,DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime DateBirth { get; set; }
        public string Address { get; set; }

        [Required] [EmailAddress] public string Email { get; set; }

        [Display(Name = "Phone number")]
        [RegularExpression(@"([03|08|07])+([0-9]{8})")]
        public string PhoneNumber { get; set; }

        public string StatusMessage { get; set; }
        
        public string ClassRoomName { get; set; }
        public string FacultyName { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string StatusMessage { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string StatusMessage { get; set; }
    }
    public class ListUserViewModel
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Sex { get; set; }
        public string PhoneNumber { get; set; }
        public string IsActive { get; set; }
    }
    public class ListStudentViewModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Sex { get; set; }
        public string PhoneNumber { get; set; }
        public string IsActive { get; set; }
    }
    public class ListTeacherViewModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Sex { get; set; }
        public string PhoneNumber { get; set; }
        public string IsActive { get; set; }
    }

    public class TranscriptsViewModel
    {
   
        public float ComponentScore { get; set; }
        public float FinalExam0 { get; set; }

        //can be null
        public float? FinalExam1 { get; set; }
        public float ScoreByScale010 { get; set; }
        public float ScoreByScale04 { get; set; }
        public string ScoreByabcdf { get; set; }
        //discipline
        public string DisCiplineName { get; set; }
    }

    public class EditUserViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string RoleName { get; set; }
        
        [Required]
        public string FullName { get; set; }
        public string Sex { get; set; }
        [Required] public string Address { get; set; }
        public DateTime DateBirth { get; set; }
        public bool IsActive { get; set; } 

        [RegularExpression(@"([03|08|07])+([0-9]{8})")]
        public string PhoneNumber { get; set; }
        
        public int? ClassRoomId { get; set; }
        
        public int? FacultyId { get; set; }
        
    }
    
    public class DeleteUserViewModel
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
    }
}