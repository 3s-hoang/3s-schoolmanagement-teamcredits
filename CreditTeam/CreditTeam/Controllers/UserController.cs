﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.Models.Entity.User;
using CreditTeam.Models.ViewModel.School;
using CreditTeam.Models.ViewModel.User;
using CreditTeam.Service.SchoolService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CreditTeam.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly TeamCreditDbContext _context;
        private readonly IPointService _pointService;
        private readonly IDisciplineService _disciplineService;
        private readonly IClassRoomService _classRoomService;
        private readonly IFacultyService _facultyService;

        public UserController(UserManager<User> userManager, RoleManager<Role> roleManager, TeamCreditDbContext context,
            IPointService pointService, IDisciplineService disciplineService, IClassRoomService classRoomService,
            IFacultyService facultyService)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _context = context;
            _pointService = pointService;
            _disciplineService = disciplineService;
            _classRoomService = classRoomService;
            _facultyService = facultyService;
        }

        // GET
        [Authorize(Policy = "writepolicy")]
        public IActionResult Index()
        {
            var user = from userlist in _userManager.Users.ToList()
                select new ListUserViewModel
                {
                    Id = userlist.Id,
                    FullName = userlist.FullName,
                    Email = userlist.Email,
                    Sex = userlist.Sex,
                    PhoneNumber = userlist.PhoneNumber,
                    IsActive = userlist.IsActive ? "Active" : "No Active"
                };
            return View(user);
        }

        [Authorize(Policy = "writepolicy")]
        public async Task<IActionResult> EditUser(int id)
        {
            ViewData["roles"] = _roleManager.Roles.ToList();
            IEnumerable<ClassRoom> classroom = _classRoomService.GetAll();
            ViewData["ClassRoomId"] = new SelectList(classroom, "Id", "Name");
            IEnumerable<Faculty> faculty = _facultyService.GetAll();
            ViewData["FacultyId"] = new SelectList(faculty, "Id", "Name");
            var user = await _userManager.FindByIdAsync(id.ToString());
            var userClaims = await _userManager.GetClaimsAsync(user);
            var userRoles = await _userManager.GetRolesAsync(user);
            var model = new EditUserViewModel
            {
                UserName = user.UserName,
                Email = user.Email,
                FullName = user.FullName,
                Sex = user.Sex,
                Address = user.Address,
                DateBirth = user.DateOfBirth,
                IsActive = user.IsActive,
                PhoneNumber = user.PhoneNumber,
                ClassRoomId = user.ClassRoomId,
                FacultyId = user.FacultyId
            };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "writepolicy")]
        public async Task<IActionResult> EditUser(EditUserViewModel editUserViewModel)
        {
            var user = await _userManager.FindByIdAsync(editUserViewModel.Id.ToString());
            user.FullName = editUserViewModel.FullName;
            user.Email = editUserViewModel.Email;
            user.FullName = editUserViewModel.FullName;
            user.Sex = editUserViewModel.Sex;
            user.DateOfBirth = editUserViewModel.DateBirth;
            user.IsActive = editUserViewModel.IsActive;
            user.ClassRoomId = editUserViewModel.ClassRoomId;
            user.FacultyId = editUserViewModel.FacultyId;
            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(editUserViewModel);
        }
        
        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public async Task<IActionResult> DeleteUser(DeleteUserViewModel deleteUserViewModel)
        {
            var user = await _userManager.FindByIdAsync(deleteUserViewModel.Id.ToString());
            user.IsActive = false;
            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction("Index");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(deleteUserViewModel);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "writepolicy")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            Models.Entity.User.User user = await _userManager.FindByIdAsync(id.ToString());
            user.IsActive = false;
            await _userManager.UpdateAsync(user);
            return RedirectToAction("Index");
        }

        [Authorize(Policy = "writepolicy")]
        public IActionResult Student()
        {
            var liststudent = from student in _userManager.Users.ToList()
                join studentrole in _context.UserRoles on student.Id equals studentrole.UserId
                join role in _roleManager.Roles.ToList() on studentrole.RoleId equals role.Id
                where role.Name == "Student"
                select new ListStudentViewModel
                {
                    FullName = student.FullName,
                    Email = student.Email,
                    Sex = student.Sex,
                    PhoneNumber = student.PhoneNumber,
                    IsActive = student.IsActive ? "Active" : "No Active"
                };
            return View(liststudent.ToList());
        }

        [Authorize(Policy = "writepolicy")]
        public IActionResult Teacher()
        {
            var liststudent = from student in _userManager.Users.ToList()
                join studentrole in _context.UserRoles on student.Id equals studentrole.UserId
                join role in _roleManager.Roles.ToList() on studentrole.RoleId equals role.Id
                where role.Name == "Teacher"
                select new ListTeacherViewModel
                {
                    FullName = student.FullName,
                    Email = student.Email,
                    Sex = student.Sex,
                    PhoneNumber = student.PhoneNumber,
                    IsActive = student.IsActive ? "Active" : "No Active"
                };
            return View(liststudent.ToList());
        }

        [Authorize(Policy = "transcriptspolicy")]
        public async Task<IActionResult> Transcripts(string id)
        {
            var userserach = await _userManager.FindByIdAsync(id);
            var pointofuser = from point in _pointService.GetAll()
                join user in _userManager.Users.ToList() on point.UserId equals user.Id
                where user.Id == userserach.Id
                select new TranscriptsViewModel
                {
                    ComponentScore = point.ComponentScore,
                    FinalExam0 = point.FinalExam0,
                    FinalExam1 = point.FinalExam1,
                    ScoreByScale010 = point.ScoreByScale010,
                    ScoreByScale04 = point.ScoreByScale04,
                    ScoreByabcdf = point.ScoreByabcdf,
                    DisCiplineName =  point.Discipline.Name
                };
            return View(pointofuser.ToList());
        }
    }
}