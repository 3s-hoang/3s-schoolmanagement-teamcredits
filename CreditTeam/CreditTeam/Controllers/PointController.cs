﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CreditTeam.Models.Entity.School;
using CreditTeam.Models.Entity.User;
using CreditTeam.Models.ViewModel.School;
using CreditTeam.Service.SchoolService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CreditTeam.Controllers
{
    [Authorize]
    public class PointController : Controller
    {
        private readonly IPointService _pointService;
        private readonly IDisciplineService _disciplineService;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;

        public PointController(IPointService pointService, IDisciplineService disciplineService,
            UserManager<User> userManager, IMapper mapper)
        {
            _pointService = pointService;
            _disciplineService = disciplineService;
            _userManager = userManager;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize(Policy = "pointpolicy")]
        public IActionResult Index()
        {
            var listpoint = from point in _pointService.GetAll()
                join user in _userManager.Users.ToList() on point.UserId equals user.Id
                select new ListPointViewModel
                {
                    Id = point.Id,
                    ComponentScore = point.ComponentScore,
                    FinalExam0 = point.FinalExam0,
                    FinalExam1 = point.FinalExam1,
                    ScoreByScale010 = point.ScoreByScale010,
                    ScoreByScale04 = point.ScoreByScale04,
                    ScoreByabcdf = point.ScoreByabcdf,
                    DisciplineName = point.Discipline.Name,
                    StudentName = user.FullName
                };
            return View(listpoint.ToList());
        }

        [HttpGet]
        [Authorize(Policy = "pointpolicy")]
        public IActionResult Create()
        {
            IEnumerable<Discipline> disciplines = _disciplineService.GetAll();
            ViewData["DisciplineId"] = new SelectList(disciplines, "Id", "Name");
            IEnumerable<User> users = _userManager.Users.ToList();
            ViewData["UserId"] = new SelectList(users, "Id", "FullName");
            return View();
        }

        [HttpPost]
        [Authorize(Policy = "pointpolicy")]
        public IActionResult Create(AddEditPointViewModel pointViewModel)
        {
            if (ModelState.IsValid)
            {
                var point = _mapper.Map<Point>(pointViewModel);
                _pointService.Add(point);
                _pointService.Save();
                return RedirectToAction("Index");
            }

            IEnumerable<Discipline> disciplines = _disciplineService.GetAll();
            ViewData["DisciplineId"] = new SelectList(disciplines, "Id", "Name");
            IEnumerable<User> users = _userManager.Users.ToList();
            ViewData["UserId"] = new SelectList(users, "Id", "FullName");
            return View();
        }

        [HttpGet]
        [Authorize(Policy = "pointpolicy")]
        public IActionResult Edit(int id)
        {
            var point = _pointService.GetById(id);
            var model = _mapper.Map<EditPointViewModel>(point);
            return View(model);
        }

        [HttpPost]
        [Authorize(Policy = "pointpolicy")]
        public IActionResult Edit(EditPointViewModel pointViewModel)
        {
            var point = _mapper.Map<Point>(pointViewModel);
            _pointService.Update(point);
            _pointService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Policy = "pointpolicy")]
        public IActionResult Delete(int id)
        {
            _pointService.Delete(id);
            _pointService.Save();
            return RedirectToAction("Index");
        }
    }
}