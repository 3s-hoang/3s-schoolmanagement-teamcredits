﻿using System.Collections.Generic;
using AutoMapper;
using CreditTeam.Models.Entity.School;
using CreditTeam.Models.ViewModel.School;
using CreditTeam.Service.SchoolService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CreditTeam.Controllers
{
    [Authorize]
    public class DisciplineController : Controller

    {
        private readonly IDisciplineService _disciplineService;
        private readonly ISemesterService _semesterService;
        private readonly IMapper _mapper;

        public DisciplineController(IDisciplineService disciplineService, ISemesterService semesterService,
            IMapper mapper)
        {
            _disciplineService = disciplineService;
            _semesterService = semesterService;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Index()
        {
            var discipline = _disciplineService.GetAll();
            var model = _mapper.Map<IEnumerable<ListDisciplineViewModel>>(discipline);
            return View(model);
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Create()
        {
            IEnumerable<Semester> semesters = _semesterService.GetAll();
            ViewData["SemesterId"] = new SelectList(semesters, "Id", "Name");
            return View();
        }

        [HttpPost]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Create(AddEditDisciplineViewModel disciplineViewModel)
        {
            if (ModelState.IsValid)
            {
                var discipline = _mapper.Map<Discipline>(disciplineViewModel);
                _disciplineService.Add(discipline);
                _disciplineService.Save();
                return RedirectToAction("Index");
            }

            IEnumerable<Semester> semesters = _semesterService.GetAll();
            ViewData["SemesterId"] = new SelectList(semesters, "Id", "Name");
            return View();
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Edit(int id)
        {
            var discipline = _disciplineService.GetById(id);
            IEnumerable<Semester> semesters = _semesterService.GetAll();
            ViewData["SemesterId"] = new SelectList(semesters, "Id", "Name", discipline.Id);
            var model = _mapper.Map<AddEditDisciplineViewModel>(discipline);
            return View(model);
        }

        [HttpPost]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Edit(AddEditDisciplineViewModel disciplineViewModel)
        {
            var discipline = _mapper.Map<Discipline>(disciplineViewModel);
            _disciplineService.Update(discipline);
            _disciplineService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Delete(int id)
        {
            _disciplineService.Delete(id);
            _disciplineService.Save();
            return RedirectToAction("Index");
        }
    }
}