﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using CreditTeam.Models;
using Microsoft.AspNetCore.Authorization;

namespace CreditTeam.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        [Authorize(Policy = "readonlypolicy")]
        public IActionResult Index()
        {
            return View();
        }
        [Authorize(Policy = "readonlypolicy")]
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}