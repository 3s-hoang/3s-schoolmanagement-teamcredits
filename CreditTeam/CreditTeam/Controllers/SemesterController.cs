﻿using System.Collections.Generic;
using AutoMapper;
using CreditTeam.Models.Entity.School;
using CreditTeam.Models.ViewModel.School;
using CreditTeam.Service.SchoolService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CreditTeam.Controllers
{
    [Authorize]
    public class SemesterController : Controller
    {
        private readonly ISemesterService _semesterService;
        private readonly IMapper _mapper;

        public SemesterController(ISemesterService semesterService, IMapper mapper)
        {
            _semesterService = semesterService;
            _mapper = mapper;
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Index()
        {
            var semester = _semesterService.GetAll();
            var model = _mapper.Map<IEnumerable<ListSemesterViewModel>>(semester);
            return View(model);
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Create(AddEditSemesterViewModel semesterViewModel)
        {
            if (ModelState.IsValid)
            {
                var semester = _mapper.Map<Semester>(semesterViewModel);
                _semesterService.Add(semester);
                _semesterService.Save();
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Edit(int id)
        {
            var semester = _semesterService.GetById(id);
            var model = _mapper.Map<AddEditSemesterViewModel>(semester);
            return View(model);
        }

        [HttpPost]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Edit(AddEditSemesterViewModel semesterViewModel)
        {
            var semester = _mapper.Map<Semester>(semesterViewModel);
            _semesterService.Update(semester);
            _semesterService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Delete(int id)
        {
            _semesterService.Delete(id);
            _semesterService.Save();
            return RedirectToAction("Index");
        }
    }
}