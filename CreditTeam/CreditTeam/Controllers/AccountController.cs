﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CreditTeam.Models.Entity.School;
using CreditTeam.Models.Entity.User;
using CreditTeam.Models.ViewModel.User;
using CreditTeam.Service.SchoolService;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CreditTeam.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IClassRoomService _classRoomService;
        private readonly IFacultyService _facultyService;

        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager,
            RoleManager<Role> roleManager, IClassRoomService classRoomService, IFacultyService facultyService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _classRoomService = classRoomService;
            _facultyService = facultyService;
        }

        [TempData] public string StatusMessage { get; set; }


        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Register(string returnUrl = null)
        {
            // pass the Role List using ViewData
            ViewData["ReturnUrl"] = returnUrl;
            ViewData["roles"] = _roleManager.Roles.ToList();
            IEnumerable<ClassRoom> classroom = _classRoomService.GetAll();
            ViewData["ClassRoomId"] = new SelectList(classroom, "Id", "Name");
            IEnumerable<Faculty> faculty = _facultyService.GetAll();
            ViewData["FacultyId"] = new SelectList(faculty, "Id", "Name");
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "writepolicy")]
        public async Task<IActionResult> Register(RegisterViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            // search role
            var role = _roleManager.FindByIdAsync(model.RoleName).Result;
            if (ModelState.IsValid)
            {
                var user = new User
                {
                    UserName = model.UserName,
                    Email = model.Email,
                    FullName = model.FullName,
                    Sex = model.Sex,
                    Address = model.Address,
                    DateOfBirth = model.DateBirth,
                    IsActive = model.IsActive,
                    PhoneNumber = model.PhoneNumber,
                    Image = null,
                    ClassRoomId = model.ClassRoomId,
                    FacultyId = model.FacultyId
                };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    // code for adding user to role
                    await _userManager.AddToRoleAsync(user, role.Name);
                    // ends here
//                    await _signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }

                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> Login(string returnUrl = null)
        {
            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This does not count login failures towards account lockout
                // To enable password failures to trigger account lockout,
                // set lockoutOnFailure: true
                var result = await _signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe,
                    lockoutOnFailure: false);
                var user = await _userManager.FindByNameAsync(model.UserName);
                if (result.Succeeded && user.IsActive == true)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ViewBag.errorUser = "username or password no true";
                    ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                    return View(model);
                }
            }

            // If execution got this far, something failed, redisplay the form.
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login");
        }

       
        
        [HttpGet]
        [Authorize(Policy = "userprofilepolicy")]
        public async Task<IActionResult> UserProfileDetail(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var model = (from userprofile in _userManager.Users.ToList()
                join classroom in _classRoomService.GetAll() on userprofile.ClassRoomId equals classroom.Id
                join faculty in _facultyService.GetAll() on userprofile.FacultyId equals faculty.Id into ur
                from userprofilecomple in ur.DefaultIfEmpty() where user.Id == userprofile.Id
                select new UserProfileViewModel
                {
                    Id = user.Id,
                    FullName = user.FullName,
                    UserName = user.UserName,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                    DateBirth = user.DateOfBirth,
                    Address = user.Address,
                    Sex = user.Sex,
                    ClassRoomName = (user.ClassRoomId == null) ? "" : user.ClassRoom.Name,
                    FacultyName = (user.FacultyId == null) ? "" : user.Faculty.Name
                }).ToList();

            return View(model);
        }

        
        
        public async Task<IActionResult> AjaxUserDetail(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var model = (from userprofile in _userManager.Users.ToList()
                join classroom in _classRoomService.GetAll() on userprofile.ClassRoomId equals classroom.Id
                join faculty in _facultyService.GetAll() on userprofile.FacultyId equals faculty.Id into ur
                from userprofilecomple in ur.DefaultIfEmpty() where userprofile.Id == user.Id
                select new UserProfileViewModel
                {
                    Id = user.Id,
                    FullName = user.FullName,
                    UserName = user.UserName,
                    Email = user.Email,
                    PhoneNumber = user.PhoneNumber,
                    DateBirth = user.DateOfBirth,
                    Address = user.Address,
                    Sex = user.Sex,
                    ClassRoomName = (user.ClassRoomId == null) ? "" : user.ClassRoom.Name,
                    FacultyName = (user.FacultyId == null) ? "" : user.Faculty.Name
                }).ToList();

            return Json(model);
        }
        
        
        
      
        [HttpGet]
        [Authorize(Policy = "userprofilepolicy")]
        public async Task<IActionResult> UserProfile(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var model = new UserProfileViewModel
            {
                FullName = user.FullName,
                UserName = user.UserName,
                Email = user.Email,
                DateBirth = user.DateOfBirth,
                PhoneNumber = user.PhoneNumber,
                StatusMessage = StatusMessage,
                Address = user.Address,
                Sex = user.Sex
            };

            return View(model);
        }
        


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = "userprofilepolicy")]
        public async Task<IActionResult> UserProfile(UserProfileViewModel model)
        {
            var user = await _userManager.FindByIdAsync(model.Id.ToString());
            user.UserName = model.UserName;
            user.Sex = model.Sex;
            user.Email = model.Email;
            user.DateOfBirth = model.DateBirth;
            user.PhoneNumber = model.PhoneNumber;
            user.Address = model.Address;
            var result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction("Index","Home");
            }

            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(model);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> ChangePassword()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await _userManager.HasPasswordAsync(user);

            var model = new ChangePasswordViewModel {StatusMessage = StatusMessage};
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var changePasswordResult =
                await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!changePasswordResult.Succeeded)
            {
                foreach (var error in changePasswordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }

                return View(model);
            }

            await _signInManager.SignInAsync(user, isPersistent: false);
            StatusMessage = "Your password has been changed.";

            return RedirectToAction(nameof(ChangePassword));
        }
        
        public IActionResult Error404()
        {
            return View();
        }
    }
}