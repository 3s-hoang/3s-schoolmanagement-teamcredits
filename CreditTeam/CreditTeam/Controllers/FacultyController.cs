﻿using System.Collections.Generic;
using AutoMapper;
using CreditTeam.Models.Entity.School;
using CreditTeam.Models.ViewModel.School;
using CreditTeam.Service.SchoolService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace CreditTeam.Controllers
{
    [Authorize]
    public class FacultyController : Controller
    {
        private readonly IFacultyService _facultyService;
        private readonly IClassRoomService _classRoomService;
        private readonly IMapper _mapper;

        public FacultyController(IFacultyService facultyService, IClassRoomService classRoomService, IMapper mapper)
        {
            _facultyService = facultyService;
            _classRoomService = classRoomService;
            _mapper = mapper;
        }


        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Index()
        {
            var faculities = _facultyService.GetAll();
            var model = _mapper.Map<IEnumerable<ListFacultyViewModel>>(faculities);
            return View(model);
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Create()
        {
            IEnumerable<ClassRoom> classrooms = _classRoomService.GetAll();
            ViewData["ClassRoomId"] = new SelectList(classrooms, "Id", "Name");
            return View();
        }

        [HttpPost]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Create(AddEditFacultyViewModel facultyViewModel)
        {
            if (ModelState.IsValid)
            {
                var faculties = _mapper.Map<Faculty>(facultyViewModel);
                _facultyService.Add(faculties);
                _facultyService.Save();
                return RedirectToAction("Index");
            }

            IEnumerable<ClassRoom> classrooms = _classRoomService.GetAll();
            ViewData["ClassRoomId"] = new SelectList(classrooms, "Id", "Name");
            return View();
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Edit(int id)
        {
            var faculties = _facultyService.GetById(id);
            IEnumerable<ClassRoom> classrooms = _classRoomService.GetAll();
            ViewData["ClassRoomId"] = new SelectList(classrooms, "Id", "Name", faculties.Id);
            var model = _mapper.Map<AddEditFacultyViewModel>(faculties);
            return View(model);
        }

        [HttpPost]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Edit(AddEditFacultyViewModel facultyViewModel)
        {
            var faculties = _mapper.Map<Faculty>(facultyViewModel);
            _facultyService.Update(faculties);
            _facultyService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Delete(int id)
        {
            _facultyService.Delete(id);
            _facultyService.Save();
            return RedirectToAction("Index");
        }
    }
}