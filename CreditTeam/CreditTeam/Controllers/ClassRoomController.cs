﻿using System.Collections.Generic;
using AutoMapper;
using CreditTeam.Models.Entity.School;
using CreditTeam.Models.ViewModel.School;
using CreditTeam.Service.SchoolService;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CreditTeam.Controllers
{
    [Authorize]
    public class ClassRoomController : Controller
    {
        private readonly IClassRoomService _classRoomService;
        private readonly IMapper _mapper;

        public ClassRoomController(IClassRoomService classRoomService, IMapper mapper)
        {
            _classRoomService = classRoomService;
            _mapper = mapper;
        }

        // GET
        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Index()
        {
            var classrooms = _classRoomService.GetAll();
            var model = _mapper.Map<IEnumerable<ClassRoomListViewModel>>(classrooms);
            return View(model);
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Create(ClassRoomAddEditViewModel classRoomViewModel)
        {
            if (ModelState.IsValid)
            {
                var classroom = _mapper.Map<ClassRoom>(classRoomViewModel);
                _classRoomService.Add(classroom);
                _classRoomService.Save();
                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Edit(int id)
        {
            var classroom = _classRoomService.GetById(id);
            var model = _mapper.Map<ClassRoomAddEditViewModel>(classroom);
            return View(model);
        }

        [HttpPost]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Edit(ClassRoomAddEditViewModel classRoomViewModel)
        {
            var classroom = _mapper.Map<ClassRoom>(classRoomViewModel);
            _classRoomService.Update(classroom);
            _classRoomService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        [Authorize(Policy = "writepolicy")]
        public IActionResult Delete(int id)
        {
            _classRoomService.Delete(id);
            _classRoomService.Save();
            return RedirectToAction("Index");
        }
    }
}