/*DrawcolContentLeft = function (id) {
    $.ajax({
        url: "/Account/AjaxUserDetail" + id,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            loadDetailUser(data);
        }
    });

    
};*/



DrawcolCenter = function (id) {
    $.ajax({
        url: "/Account/AjaxUserDetail/" + id,
        type: "GET",
        dataType: "json",
        contentType: "application/json",
        success: function (data) {
            loadDetailUser1(data);
            loadDetailUser2(data);
        }
    });

    function loadDetailUser1(data) {
        var table = $("#detailFulName");
        $(table).empty();
        $.each(data, function (index, value) {
            $(table).append(
                '<h3> ' + value.fullName + ' </h3>' +
                '<h4>Khóa : ' + value.classRoomName + ' </h4>' +
                '<h4>Khoa : ' + value.facultyName + ' </h4>'
            );
        });
    }

    function loadDetailUser2(data) {
        var table = $("#detailUser");
        $(table).empty();
        $.each(data, function (index, value) {
            $(table).append(
                '<div class="right-divider hidden-sm hidden-xs">' +
                '<table>' +
                '<tr>' +
                '<td>' +
                '<h4>Mã SV</h4>' +
                '</td>' +
                '<td class="viewprofile">' + value.userName + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<h4>Giới tính:</h4>' +
                '</td>' +
                '<td class="viewprofile">' + value.sex + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<h4>Ngày sinh:</h4>' +
                '</td>' +
                '<td class="viewprofile">' + value.dateBirth + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                ' <h4>Số Điện Thoại:</h4>' +
                '</td>' +
                '<td class="viewprofile">' + value.phoneNumber + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<h4>Email:</h4>' +
                '</td>' +
                '<td class="viewprofile">' + value.email + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>' +
                '<h4>Address:</h4>' +
                ' </td>' +
                '<td class="viewprofile">' + value.address + '</td>' +
                '</tr>' +
                '</table>' +
                '</div>' +
                '</div>'
            );
        });
    }
};

init = function(){
    var id = $('#userId').val(); 
 /*   DrawcolContentLeft(id);*/
    DrawcolCenter(id);
};

$(document).ready(function () {
    init();
});
