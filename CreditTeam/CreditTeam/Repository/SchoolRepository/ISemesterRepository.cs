﻿using CreditTeam.Models.Entity.School;
using CreditTeam.Repository.GenericRepository;

namespace CreditTeam.Repository.SchoolRepository
{
    public interface ISemesterRepository : IGenericRepository<Semester>
    {
        
    }
}