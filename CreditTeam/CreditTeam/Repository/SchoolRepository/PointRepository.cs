﻿using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.Repository.GenericRepository;

namespace CreditTeam.Repository.SchoolRepository
{
    public class PointRepository : GenericRepository<Point> , IPointRepository
    {
        public PointRepository(TeamCreditDbContext context) : base(context)
        {
            
        }
    }
}