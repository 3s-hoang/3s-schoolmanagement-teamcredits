﻿using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.Repository.GenericRepository;

namespace CreditTeam.Repository.SchoolRepository
{
    public class ClassRoomRepository : GenericRepository<ClassRoom>, IClassRoomRepository
    {
        public ClassRoomRepository(TeamCreditDbContext context) : base(context)
        {
            
        }
    }
}