﻿using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.Repository.GenericRepository;

namespace CreditTeam.Repository.SchoolRepository
{
    public class FacultyRepository : GenericRepository<Faculty> , IFacultyRepository
    {
        public FacultyRepository(TeamCreditDbContext context) : base (context)
        {
            
        }
    }
}