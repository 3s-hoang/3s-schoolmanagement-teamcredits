﻿using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.Repository.GenericRepository;

namespace CreditTeam.Repository.SchoolRepository
{
    public class DisciplineRepository : GenericRepository<Discipline> , IDisciplineRepository
    {
        public DisciplineRepository(TeamCreditDbContext context) : base(context)
        {
            
        }
    }
}