﻿using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.Repository.GenericRepository;

namespace CreditTeam.Repository.SchoolRepository
{
    public class SemesterRepository : GenericRepository<Semester> , ISemesterRepository
    {
        public SemesterRepository(TeamCreditDbContext context) : base(context)
        {
            
        }
    }
}