﻿using System;
using CreditTeam.Models;
using CreditTeam.Models.Entity.School;
using CreditTeam.Repository.GenericRepository;

namespace CreditTeam.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private TeamCreditDbContext _context;

        public UnitOfWork(TeamCreditDbContext context)
        {
            _context = context;
            InitReponsitory();
        }

        private bool _disposed;
        
        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IGenericRepository<ClassRoom> ClassRoomRepository { get; private set; }
        public IGenericRepository<Discipline> DisciplineRepository { get; private set; }
        public IGenericRepository<Faculty> FacultyRepository { get; private set; }
        public IGenericRepository<Point> PointRepository { get; private set; }
        public IGenericRepository<Semester> SemesterRepository { get; private set; }
        

        private void InitReponsitory()
        {
            ClassRoomRepository = new GenericRepository<ClassRoom>(_context);
            DisciplineRepository = new GenericRepository<Discipline>(_context);
            FacultyRepository = new GenericRepository<Faculty>(_context);
            PointRepository = new GenericRepository<Point>(_context);
            SemesterRepository = new GenericRepository<Semester>(_context);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}