﻿using System;
using CreditTeam.Models.Entity.School;
using CreditTeam.Repository.GenericRepository;

namespace CreditTeam.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<ClassRoom> ClassRoomRepository { get; }
        IGenericRepository<Discipline> DisciplineRepository { get; }
        IGenericRepository<Faculty> FacultyRepository { get; }
        IGenericRepository<Point> PointRepository { get; }
        IGenericRepository<Semester> SemesterRepository { get; }
        void Save();
    }
}